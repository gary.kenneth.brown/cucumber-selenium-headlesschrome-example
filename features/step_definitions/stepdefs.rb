require 'selenium-webdriver'
options = Selenium::WebDriver::Chrome::Options.new(args: ['headless','disable-gpu','no-sandbox'])
driver = Selenium::WebDriver.for :chrome, options: options

Given("google is displayed") do
  driver.navigate.to "http://www.google.co.uk"
end

When("I search for NHSBSA") do
  element = driver.find_element(name: 'q')
  element.send_keys("NHSBSA")
  element.submit
end

Then("I should see results") do
  driver.manage.window.resize_to(1024, 900)
  driver.save_screenshot "google-nhsbsa-screenshot.png"
  driver.quit
end

