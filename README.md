# cucumber-selenium-headlesschrome-example
Simple project demonstrating implementing headless Chrome automated testing in GitLab CI.
It uses standard NHSBSA test tooling for the definition and execution of tests:
* Cucumber
* Ruby
* Selenium

The test report and screenshots will be output as GitLab CI artifacts at the end of each execution.

# How does it work?
This is a very simple Cucumber / Ruby project which goes to Google and searches for NHSBSA and finally takes a screenshot to show the successful result.
The Chrome browser is used by the Cucumber/Ruby tests (via Selenium) in the same desktop or GitLab runner instance; so no need to connect to any grids or external services other than the application being tested in dev/test.

The step definition configures the selenium driver for Chrome headless:
<https://gitlab.com/gary.kenneth.brown/cucumber-selenium-headlesschrome-example/blob/master/features/step_definitions/stepdefs.rb>

```
options = Selenium::WebDriver::Chrome::Options.new(args: ['headless','disable-gpu','no-sandbox'])
driver = Selenium::WebDriver.for :chrome, options: options
```

The parts which are key are `disable-gpu` and `no-sandbox` as these won’t work in a Docker VM container - aka GitLab runners.
If Watir is used as an abstraction it would be very similar to the above to configure. The Watir manual suggests:

```
b = Watir::Browser.new :chrome, headless: true
```

Everything else remains the same in the test projects currently as it is just the initial configuration of the Selenium / Watir frameworks.
The project can be run locally or in a GitLab runner provided the required tools are present:
* Ruby
* Cucumber
* Chrome


# TODO
1. The script downloads and installs, Chrome and the Selenium Chrome Driver on each run which is not efficient.
Reuse or create a GitLab Docker image with this setup pre-configured.

2. Integration of tests into a development and test dashboard.